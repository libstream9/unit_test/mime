#include "src/xml/mime_info.hpp"

#include "src/xml/mime_type.hpp"

#include <boost/test/unit_test.hpp>

namespace testing {

namespace xml = stream9::xdg::mime::xml;

BOOST_AUTO_TEST_SUITE(mime_)
BOOST_AUTO_TEST_SUITE(xml_)

BOOST_AUTO_TEST_SUITE(mime_info_)

    BOOST_AUTO_TEST_CASE(construct_)
    {
        xml::mime_info info;

        BOOST_TEST(info.to_xml() ==
            "<mime-info xmlns=\"http://www.freedesktop.org/standards/shared-mime-info\"/>\n\n");
    }

    BOOST_AUTO_TEST_CASE(push_back_)
    {
        xml::mime_info info;

        {
            xml::mime_type mt1 { "test/type1" , "comment1" };
            xml::mime_type mt2 { "test/type2" , "comment2" };

            BOOST_TEST(info.size() == 0);

            info.push_back(mt1);
            info.push_back(mt2);

            BOOST_TEST(info.size() == 2);
        }
    }

BOOST_AUTO_TEST_SUITE_END() // mime_info_

BOOST_AUTO_TEST_SUITE_END() // xml_
BOOST_AUTO_TEST_SUITE_END() // mime_

} // namespace testing
