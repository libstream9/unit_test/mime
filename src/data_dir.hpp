#ifndef STREAM9_XDG_TEST_MIME_DATA_DIR_HPP
#define STREAM9_XDG_TEST_MIME_DATA_DIR_HPP

#include "namespace.hpp"

#include <string>

#include <boost/preprocessor/stringize.hpp>

#include <stream9/path/concat.hpp> // operator/

namespace testing {

inline std::string
data_dir()
{
    return std::string(BOOST_PP_STRINGIZE(DATA_DIR));
}

inline std::string
mime_data_dir()
{
    using stream9::path::operator/;

    return data_dir() / "mime";
}

} // namespace testing

#endif // STREAM9_XDG_TEST_MIME_DATA_DIR_HPP
