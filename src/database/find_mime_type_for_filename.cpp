#include <stream9/xdg/mime/database.hpp>

#include "../data_directory.hpp"
#include "../make_paths.hpp"
#include "../namespace.hpp"

#include "src/xml/mime_type.hpp"

#include <boost/test/unit_test.hpp>

#include <stream9/test/scoped_env.hpp>

namespace testing {

BOOST_AUTO_TEST_SUITE(database_)
BOOST_AUTO_TEST_SUITE(find_mime_type_for_filename_)

    BOOST_AUTO_TEST_CASE(no_match_)
    {
        data_directory dir1;

        auto const& path1 = make_paths(dir1.path());
        test::scoped_env env { "XDG_DATA_DIRS", path1 };

        dir1.update_database();

        mime::database db;

        auto const& mt = db.find_mime_type_for_filename("xxx.foo");
        BOOST_TEST(mt == "application/octet-stream");
    }

    BOOST_AUTO_TEST_CASE(single_match_)
    {
        data_directory dir1;

        auto const& p1 = make_paths(dir1.path());
        test::scoped_env env { "XDG_DATA_DIRS", p1 };

        mime::xml::mime_type mt1 { "foo/bar", "comment" };
        mt1.append_glob("*.foo");
        dir1.append(mt1);
        dir1.update_database();

        mime::database db;

        auto const& mt = db.find_mime_type_for_filename("xxx.foo");
        BOOST_TEST(mt == "foo/bar");
    }

    BOOST_AUTO_TEST_CASE(multiple_glob_match_biggest_weight_get_choosen_)
    {
        data_directory dir1;

        auto const& p1 = make_paths(dir1.path());
        test::scoped_env env { "XDG_DATA_DIRS", p1 };

        mime::xml::mime_type mt1 { "test/type1", "comment" };
        mt1.append_glob("*.x.y", 50);
        dir1.append(mt1);

        mime::xml::mime_type mt2 { "test/type2", "comment" };
        mt2.append_glob("*.y", 60);
        dir1.append(mt2);

        dir1.update_database();

        mime::database db;

        auto const& candidates = db.find_mime_types_for_filename("foo.x.y");
        BOOST_TEST(candidates.size() == 2);

        auto const& mt = db.find_mime_type_for_filename("foo.x.y");
        BOOST_TEST(mt == "test/type2");
    }

    BOOST_AUTO_TEST_CASE(multiple_glob_match_longest_pattern_get_choosen_)
    {
        data_directory dir1;

        auto const& p1 = make_paths(dir1.path());
        test::scoped_env env { "XDG_DATA_DIRS", p1 };

        mime::xml::mime_type mt1 { "test/type1", "comment" };
        mt1.append_glob("*.x.y");
        dir1.append(mt1);

        mime::xml::mime_type mt2 { "test/type2", "comment" };
        mt2.append_glob("*.y");
        dir1.append(mt2);

        dir1.update_database();

        mime::database db;

        auto const& candidates = db.find_mime_types_for_filename("foo.x.y");
        BOOST_TEST(candidates.size() == 2);

        auto const& mt = db.find_mime_type_for_filename("foo.x.y");
        BOOST_TEST(mt == "test/type1");
    }

    BOOST_AUTO_TEST_CASE(multiple_glob_match_all_resolve_to_same_mime_type_)
    {
        data_directory dir1, dir2;

        auto const& p1 = make_paths(dir1.path(), dir2.path());
        test::scoped_env env { "XDG_DATA_DIRS", p1 };

        mime::xml::mime_type mt1 { "test/type1", "comment" };
        mt1.append_glob("*.x", 50);
        dir1.append(mt1);

        dir1.update_database();

        mime::xml::mime_type mt2 { "test/type1", "comment" };
        mt2.append_glob("*.x", 50);
        dir2.append(mt2);

        dir2.update_database();

        mime::database db;

        auto const& mt = db.find_mime_type_for_filename("foo.x");
        BOOST_TEST(mt == "test/type1");
    }

    BOOST_AUTO_TEST_CASE(multiple_glob_match_no_definitive_match_)
    {
        data_directory dir1;

        auto const& p1 = make_paths(dir1.path());
        test::scoped_env env { "XDG_DATA_DIRS", p1 };

        mime::xml::mime_type mt1 { "test/type1", "comment" };
        mt1.append_glob("*.x", 50);
        dir1.append(mt1);

        mime::xml::mime_type mt2 { "test/type2", "comment" };
        mt2.append_glob("*.x", 50);
        dir1.append(mt2);

        dir1.update_database();

        mime::database db;

        auto const& candidates = db.find_mime_types_for_filename("foo.x");
        BOOST_TEST(candidates.size() == 2);

        auto const& mt = db.find_mime_type_for_filename("foo.x");
        BOOST_TEST(mt == "application/octet-stream");
    }

BOOST_AUTO_TEST_SUITE_END() // find_mime_type_for_filename_
BOOST_AUTO_TEST_SUITE_END() // database_

} // namespace testing
