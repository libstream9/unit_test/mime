#include <stream9/xdg/mime/database.hpp>

#include "../data_directory.hpp"
#include "../make_paths.hpp"
#include "../namespace.hpp"

#include "src/xml/mime_type.hpp"

#include <algorithm>

#include <boost/test/unit_test.hpp>

#include <stream9/filesystem/temporary_directory.hpp>
#include <stream9/fstream.hpp>
#include <stream9/linux/mkdir.hpp>
#include <stream9/path/concat.hpp> // operator/
#include <stream9/test/scoped_env.hpp>

namespace testing {

namespace lx = stream9::linux;

using stream9::path::operator/;

BOOST_AUTO_TEST_SUITE(database_)
BOOST_AUTO_TEST_SUITE(find_mime_types_for_filesystem_)

    BOOST_AUTO_TEST_CASE(no_match_)
    {
        data_directory dir1;

        auto const& path = make_paths(dir1.path());
        test::scoped_env env { "XDG_DATA_DIRS", path };

        dir1.update_database();

        mime::database db;

        fs::temporary_directory tmp;
        auto const& types = db.find_mime_types_for_filesystem(tmp.path().c_str());

        BOOST_TEST_REQUIRE(types.size() == 0);
    }

    BOOST_AUTO_TEST_CASE(single_match_)
    {
        using mime::xml::mime_type;

        data_directory dir1;

        auto const& path = make_paths(dir1.path());
        test::scoped_env env { "XDG_DATA_DIRS", path };

        {
            mime_type mt { "foo/bar", "comment" };

            mime_type::treematch match1 {
                "file1", // path
                "file",  // type
                "false", // match-case
                "false", // executable
                "false", // non-empty
                "",      // mime-type
                {},      // children
            };

            mime_type::treemagic magic1 {
                50,          // priority
                { match1 },  // matches
            };

            mt.append_treemagic(magic1);

            dir1.append(mt);
            dir1.update_database();
        }

        mime::database db;

        fs::temporary_directory tmp;
        st9::ofstream ofs { tmp.path() / "file1" };
        ofs << "dummy";
        ofs.close();

        auto const& types = db.find_mime_types_for_filesystem(tmp.path().c_str());

        auto const expected = { "foo/bar" };
        BOOST_TEST(types == expected, boost::test_tools::per_element());
    }

    BOOST_AUTO_TEST_CASE(multiple_match_)
    {
        using mime::xml::mime_type;

        data_directory dir1;

        auto const& path = make_paths(dir1.path());
        test::scoped_env env { "XDG_DATA_DIRS", path };

        {
            mime_type mt { "foo/bar", "comment" };

            mime_type::treematch match1 {
                "bar",   // path
                "file",  // type
                "false", // match-case
                "false", // executable
                "false", // non-empty
                "",      // mime-type
                {},      // children
            };

            mime_type::treemagic magic1 {
                50,          // priority
                { match1 },  // matches
            };

            mt.append_treemagic(magic1);

            dir1.append(mt);
            dir1.update_database();
        }
        {
            mime_type mt { "foo/baz", "comment" };

            mime_type::treematch match1 {
                "baz",   // path
                "directory",  // type
                "false", // match-case
                "false", // executable
                "false", // non-empty
                "",      // mime-type
                {},      // children
            };

            mime_type::treemagic magic1 {
                50,          // priority
                { match1 },  // matches
            };

            mt.append_treemagic(magic1);

            dir1.append(mt);
            dir1.update_database();
        }

        mime::database db;

        fs::temporary_directory tmp;
        {
            st9::ofstream ofs { tmp.path() / "bar" };
            ofs << "dummy";
            ofs.close();
        }
        {
            auto p = tmp.path() / "baz";
            lx::mkdir(p);
        }

        auto types = db.find_mime_types_for_filesystem(tmp.path().c_str());
        std::ranges::sort(types);

        auto const expected = { "foo/bar", "foo/baz" };
        BOOST_TEST(types == expected, boost::test_tools::per_element());
    }

BOOST_AUTO_TEST_SUITE_END() // find_mime_types_for_filesystem_
BOOST_AUTO_TEST_SUITE_END() // database_

} // namespace testing
