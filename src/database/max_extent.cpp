#include <stream9/xdg/mime/database.hpp>

#include "../data_directory.hpp"
#include "../make_paths.hpp"
#include "../namespace.hpp"

#include "src/xml/mime_type.hpp"

#include <boost/test/unit_test.hpp>

#include <stream9/test/scoped_env.hpp>

namespace testing {

BOOST_AUTO_TEST_SUITE(database_)
BOOST_AUTO_TEST_SUITE(max_extent_)

    BOOST_AUTO_TEST_CASE(at_least_128_byte_minimum_)
    {
        data_directory dir1;

        using mime::xml::mime_type;

        auto const& path = make_paths(dir1.path());
        test::scoped_env env { "XDG_DATA_DIRS", path };

        mime::database db;

        auto const e = db.max_extent();
        BOOST_TEST(e == 128);
    }

    BOOST_AUTO_TEST_CASE(different_value_in_multiple_directory_)
    {
        data_directory dir1, dir2;

        using mime::xml::mime_type;

        auto const& path = make_paths(dir1.path(), dir2.path());
        test::scoped_env env { "XDG_DATA_DIRS", path };

        {
            mime::xml::mime_type mt1 { "foo/bar", "comment" };
            mt1.append_magic(mime_type::magic { 40, {
                    mime_type::match { "string", "1000", "foo", "", {}, },
                }
            });
            dir1.append(mt1);
            dir1.update_database();
        }

        {
            mime::xml::mime_type mt1 { "foo/bar", "comment" };
            mt1.append_magic(mime_type::magic { 40, {
                    mime_type::match { "string", "1500", "foo", "", {}, },
                }
            });
            dir2.append(mt1);
            dir2.update_database();
        }

        mime::database db;

        auto const e = db.max_extent();
        BOOST_TEST(e == 1504);
    }

BOOST_AUTO_TEST_SUITE_END() // max_extent_
BOOST_AUTO_TEST_SUITE_END() // database_

} // namespace testing
