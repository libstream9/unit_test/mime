#ifndef STREAM9_XDG_TEST_MIME_NAMESPACE_HPP
#define STREAM9_XDG_TEST_MIME_NAMESPACE_HPP

namespace stream9::xdg::mime {}
namespace stream9::ranges {}
namespace stream9::filesystem {}
namespace stream9::test {}

namespace testing {

namespace st9 = stream9;
namespace mime = stream9::xdg::mime;
namespace test = stream9::test;

namespace rng { using namespace stream9::ranges; }
namespace fs { using namespace stream9::filesystem; }

} // namespace testing

#endif // STREAM9_XDG_TEST_MIME_NAMESPACE_HPP
